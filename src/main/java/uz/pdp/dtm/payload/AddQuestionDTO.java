package uz.pdp.dtm.payload;

import lombok.Data;

import java.util.List;

@Data
public class AddQuestionDTO {

    private String savol;

    private List<ChoiceDTO> choices;
}
