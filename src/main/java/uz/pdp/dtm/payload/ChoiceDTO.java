package uz.pdp.dtm.payload;

import jakarta.persistence.Column;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import uz.pdp.dtm.entity.Question;

@Data
public class ChoiceDTO {

    private String option;

    private boolean correct;
}
