package uz.pdp.dtm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.dtm.entity.Choice;
import uz.pdp.dtm.entity.Question;
import uz.pdp.dtm.payload.AddQuestionDTO;
import uz.pdp.dtm.payload.ChoiceDTO;
import uz.pdp.dtm.repository.ChoiceRepository;
import uz.pdp.dtm.repository.QuestionRepository;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/question")
@RequiredArgsConstructor
public class QuestionController {

    private final QuestionRepository questionRepository;
    private final ChoiceRepository choiceRepository;


    @PostMapping("/add")
    public HttpEntity<?> add(@RequestBody AddQuestionDTO addQuestionDTO) {
        Question question = Question.builder()
                .savol(addQuestionDTO.getSavol())
                .build();
        List<Choice> choices = new LinkedList<>();
        for (ChoiceDTO choiceDTO : addQuestionDTO.getChoices()) {
            Choice choice = Choice.builder()
                    .correct(choiceDTO.isCorrect())
                    .option(choiceDTO.getOption())
                    .question(question)
                    .build();
            choices.add(choice);
        }

        question.setChoices(choices);
        questionRepository.save(question);

        return ResponseEntity.ok(question);
    }
}
