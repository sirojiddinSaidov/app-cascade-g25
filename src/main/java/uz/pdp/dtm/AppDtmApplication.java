package uz.pdp.dtm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppDtmApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppDtmApplication.class, args);
    }

}
