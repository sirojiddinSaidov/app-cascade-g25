package uz.pdp.dtm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.dtm.entity.Choice;

public interface ChoiceRepository extends JpaRepository<Choice, Integer> {
}