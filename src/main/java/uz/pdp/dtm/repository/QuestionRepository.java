package uz.pdp.dtm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.dtm.entity.Question;

public interface QuestionRepository extends JpaRepository<Question, Integer> {
}